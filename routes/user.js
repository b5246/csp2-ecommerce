const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')
const auth = require('../auth')

// Route to Create User
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController))
})

// Route to Access all user (Restriction: ADMIN)
router.get("/",auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getAllUser().then(resultFromController => res.send(resultFromController));
	}else res.send('Restricted Access.')
})

//Route for Login [Authentication/Validation] : access Auth. token
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController))
})

// Route for SetAsAdmin Function (Restriction: ADMIN)
router.put("/:userId/setAsAdmin", auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.setUserAsAdmin(req.params).then(resultFromController=> res.send(resultFromController))
	}else res.send('Restricted Access.')
})

// ROute to Retrieve and update user by ID (Restriction: ADMIN)
router.put("/:userId/update", auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getUserById(req.params,req.body).then(resultFromController=> res.send(resultFromController))
	}else res.send('Restricted Access.')
})


// Route to CREATE Order (Restriction: authenticated NON-admin)
router.post("/checkout",auth.verify, (req,res)=>{
	if(!auth.decode(req.headers.authorization).isAdmin){
		const data = {
			productId: req.body.productId,
			quantity: req.body.quantity,
			userId: auth.decode(req.headers.authorization).id
		}
		userController.createOrder(data).then(resultFromController=> res.send(resultFromController))
	} else{
	 	res.send('Admin is not allowed to process Order')
	}
})

//Route to Retrieve All Orders (admin only)
router.get("/orders",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getOrders().then(resultFromController=> res.send(resultFromController))
	}else res.send('Restricted Access.')
})

//Route to Retrieve Authenticated User's Orders (NON-admin only)
router.get("/myOrders",auth.verify,(req,res)=>{
	if(!auth.decode(req.headers.authorization).isAdmin){
		const data = {userId: auth.decode(req.headers.authorization).id}
		userController.getMyOrders(data).then(resultFromController=> res.send(resultFromController))
	}else res.send('Restricted Access.')
})



//============================================>
module.exports = router
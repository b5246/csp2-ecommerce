const express = require('express')
const router = express.Router()
const productController = require('../controllers/product')
const auth = require('../auth')

// Create Product functionality [Restriction: Admin]
router.post("/",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.createProduct(req.body).then(resultFromController=> res.send(resultFromController))
	}else{res.send('Restricted Access.')}
})

// Additional Access All
router.get("/all",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.getAllProduct().then(resultFromController => res.send(resultFromController))
	}else{res.send('Restricted Access.')}	
})

// Retrieve all active products
router.get("/",auth.verify,(req,res)=>{
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController))
})

// Retrieve single product
router.get("/:productId",auth.verify,(req,res)=>{
	productController.getProductById(req.params).then(resultFromController=>res.send(resultFromController))
})

// Update Product Info [Restriction: Admin]
router.put("/:productId",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
	}else{res.send('Restricted Access.')}	
})

// Archive Product [Restriction: Admin]
router.put("/:productId/archive",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.archiveProduct(req.params).then(resultFromController=>res.send(resultFromController))
	}else{res.send('Restricted Access.')}	
})



//=============================================>
module.exports = router
const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	//user: {type: mongoose.Schema.ObjectId, ref: 'User'},
	userId: {type: String,required: [true,"User Id is required"]},
	purchasedAt:{type:Date,default: new Date()},
	status:{type:Boolean,default:true},
	totalAmount:{
		type:Number,
		required: [true,"Total Amount is required"]
	},
	OrderDetails:{
		productId: {
			type: String,
			required: [true,"Product Id is required"]},
		quantity:{
			type:Number,
			required: [true,"Quantity is required"]}
	}
})

module.exports = mongoose.model("Order", orderSchema)
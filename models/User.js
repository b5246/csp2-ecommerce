const mongoose = require('mongoose')

const userSchema= new mongoose.Schema({
	email: {
		type: String,
		trim: true,
		unique: 'Email already exists',
		match: [/.+\@.+\..+/, 'Please fill a valid email address'],
		required: [true,"Email is required"]
	},
	password: {type: String,required: [true,"Password is required"]},
	isAdmin: {
		type: Boolean,
		required: false,
		default: false},
	firstName: {
		type: String,
		trim: true,
		required: [true,"First Name is required"]
	},
	lastName: {
		type: String,
		trim: true,
		required: [true,"Last Name is required"]
	},
	telephone: {
		type: String,
		required: [true,"Mobile number is required"]
	},
	address:{
		addressLine1:{type: String,required: [true,"Street is required"]},
		addressLine2:{type: String,required: [true,"Village/ Town is required"]},
		city:{type: String,required: [true,"City is required"]},
		postal:{type: Number,required: [true,"Postal code is required"]},
		country:{type: String,required: [true,"Country is required"]}
	},
	createdAt:{
		type: Date,
    	default: Date.now
	},
	modifiedAt:Date,
	cartTotal: {type:Number,default:0},
	purchaseHistory:[{
		productId: String,
		quantity: Number,
		orderAt:{
				type: Date,
				default: Date.now
			},
		status :{
				type: String,
				default: "No Transaction"
			}
	}]
})

module.exports = mongoose.model("User", userSchema)
const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	SKU:{type:String,required: [true,"SKU is required"]},
	name: {type: String,required: [true,"Email is required"]},
	itemCategory:{type:String,required: [true,"Product Category is required"]},
	description:{type:String,required: [true,"Description is required"]},
	manufacturer:{type:String,required: [true,"Manufacturer is required"]},
	itemDetails:{
		length:{type:Number,required: [true,"Length is required"]},
		heigth:{type:Number,required: [true,"Height is required"]},
		width:{type:Number,required: [true,"Width is required"]},
		weight:{type:Number,required: [true,"Weight is required"]}
	},
	unitPrice:{type:Number,required: [true,"Price is required"]},
	stockQuantity:{type:Number,required: [true,"stock number is required"]},
	isActive:{type:Boolean,default: true},
	createdAt:{type:Date,default: Date.now},
	modifiedAt:Date,
	purchaseOrder:[{
		purchasedQty: String,
		orderAt:{
				type: Date,
				default: Date.now
			},
		status :{
				type: String,
				default: "No Transaction"
			}
	}]
})

module.exports = mongoose.model("Product", productSchema)
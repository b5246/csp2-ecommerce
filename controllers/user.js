const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require("bcrypt")
const auth = require("../auth")

// Create User 
module.exports.registerUser = data=>{
	//Check if email exist
	let newUser = new User({
		email: data.email,
		password: bcrypt.hashSync(data.password,10),
		//isAdmin: data.isAdmin,
		firstName:data.firstName,
		lastName:data.lastName,
		telephone:data.telephone,
		address:data.address
	})
	return User.findOne({email:data.email}).then(user=>{
		if(!user){
			return newUser.save().then((result,error)=>{
				if(error){
					console.log(error)
					return `Regitration Failed.`
				}
				else{return `Successfully Registered!`}
			})
			
		} else { return Promise.resolve(`Email is already registered`)}
	})
}

// Access all user
module.exports.getAllUser=()=>{
	return User.find({}).then(result => {
		return result;
	})
}

//Login [Authentication/Validation] : access Auth. token
module.exports.loginUser = data=>{
	return User.findOne({email:data.email}).then(result=>{
		if(result==null){return `${data.email} is not yet registered`}
		else{
			const isPasswordCorrect = bcrypt.compareSync(data.password,result.password)
			if(isPasswordCorrect){return{access:auth.createAccessToken(result)}} else {return `Password is incorrect`}
		}
	})
}

//SetAsAdmin Function
module.exports.setUserAsAdmin = (reqParams)=>{
	let idParam = reqParams.userId
	if(idParam.match(/^[0-9a-fA-F]{24}$/)){
		let updatedUser = {
			isAdmin: true,
			modifiedAt: Date.now()
		}
		return User.findByIdAndUpdate(reqParams.userId,updatedUser).then((result,error)=>{
			if(error){
				return false
			}else {return `User is set as Admin`}
		})
	} else{return Promise.resolve(`Invalid Parameter.`)}
	
}

// Retrieve and update user (Restriction: ADMIN)
module.exports.getUserById = (reqParams,data)=>{
	// Check if Id Parameter is Valid
	let idParam = reqParams.userId
	let userUpdate = {
		firstName: data.firstName,
		lastName:data.lastName,
		telephone: data.telephone,
		address: data.address,
		modifiedAt: Date.now()
	}
	if(idParam.match(/^[0-9a-fA-F]{24}$/)){
		return User.findByIdAndUpdate(reqParams.userId,userUpdate).then((result,error)=>{
			if(error){
				return false
			}else {return `User Details Upadted`}
		})
	}else{return Promise.resolve(`Invalid Parameter.`)}
}


//CREATE Order (Restriction: NON ADMIN)
module.exports.createOrder = async data=>{
	try{
		//Place Order log to Order database
		let prodStat = await Product.findById(data.productId).then( async result=>{
			if(result.isActive==false){
				return false
			}else{
				let userOrder = await Product.findById(data.productId).then(async result=>{
					if(result.stockQuantity !==0){
						result.updateOne({$set:{
							stockQuantity: result.stockQuantity - data.quantity
						}}).then(result=>{})

						// Update User Cart Toatl Amount
						let myCart = await	 User.findById(data.userId).then(user=>{
							user.updateOne({cartTotal: user.cartTotal +(result.unitPrice * data.quantity)}).then(result=>{})
						})

						let cart = new Order({
							userId: data.userId,
							purchasedAt: Date.now(),
							status: true,
							totalAmount: result.unitPrice * data.quantity,
							OrderDetails: {
								productId: data.productId,
								quantity: data.quantity
							}
						})	
						
						return cart.save().then((result,error)=>{
							if(error){return false
							} else{
								return true }
						})	
					} else{return false} 
				})

				// Update Product's order history
				let modifyProdduct = await Product.findById(data.productId).then(result=>{
					if (result.stockQuantity === 0){
						result.updateOne({$set:{isActive:false}}).then(result=>{})
					}else{
						result.purchaseOrder.push({
							purchasedQty:data.quantity,
							orderAt: Date.now(),
							status: "ORDER PLACED"
						})
						return result.save().then((result,error)=>{
							if(error){return false}
							else{return true}
						})
					}
				})

				//Update User's purchaseHistory
				let userHistory = await User.findById(data.userId).then(result=>{
					result.purchaseHistory.push({
						productId: data.productId,
						quantity: data.quantity,
						orderAt: Date.now(),
						status: "ORDER PLACED"
					})
					return result.save().then((result,error)=>{
						if(error){return false}
						else{return true}
					})
				})

				if(userOrder && modifyProdduct&& userHistory ){
					return true
				}else {return false}
			}
		})
		if(prodStat){
			return 'Order Posted'
		} else{return `Fail to process Order: Please check stock quantity or active status`}
		
	} catch{return(`Encounter an error while processing`)}
}

//Retrieve All Orders (admin only)
module.exports.getOrders =()=>{
	return Order.find({}).then(result => {
		return result
	})
}

//Retrieve Authenticated User's Orders (NON-admin only)
module.exports.getMyOrders = data=>{
	try{
		return User.findById(data.userId).then((result,error)=>{
			let newCart = {
				userTotalAmount: result.cartTotal,
				history: result.purchaseHistory
			}
			if(error) {return 'Invalid User'}
			else {return newCart 
			}
		})
	} catch{return(`Restricted Access.`)}
}
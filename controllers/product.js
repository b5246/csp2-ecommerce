const Product = require("../models/Product")
const bcrypt = require("bcrypt")
const auth = require("../auth")

// Create Product functionality [Restriction: Admin]
module.exports.createProduct=data=>{
	let newProduct = new Product(data)
	// Check if SKU is unique
	return Product.findOne({SKU:data.SKU}).then(product=>{
		if(!product){
			return newProduct.save().then((result,error)=>{
				if(error){
					return `Regitration Failed.`
				} else{
					return `Successfully Registered!`
				}
			})
		} else{ return Promise.resolve(`Product is already registered`) }
	})
}


// Additional Access All
module.exports.getAllProduct=()=>{
	return Product.find().then(result => {
		return result;
	})
}

// Retrieve all active products
module.exports.getAllActiveProduct=()=>{
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}


// Retrieve single product
module.exports.getProductById=reqParams=>{
	// Check if Id Parameter is Valid
	let prodId = reqParams.productId
	if(prodId.match(/^[0-9a-fA-F]{24}$/)){
		return Product.findById(prodId).then((result)=>{
			if(result){ return result
			} else{return Promise.resolve(`Invalid Parameter.`)}
		})
	} else {return Promise.resolve(`Invalid Parameter.`)}
}

// Update Product Info [Restriction: Admin]
module.exports.updateProduct =(reqParams,data)=>{
	let prodId = reqParams.productId
	let prodUpdate = {
		name:data.name,
		itemCategory: data.itemCategory,
		description: data.description,
		manufacturer: data.manufacturer,
		unitPrice:data.unitPrice,
		stockQuantity: data.stockQuantity,
		modifiedAt: Date.now()
	}
	if(prodId.match(/^[0-9a-fA-F]{24}$/)){
		return Product.findByIdAndUpdate(reqParams.productId,prodUpdate).then((result,error)=>{
			if(error){
				return `Something went wrong while updating`
			}else {return `Product Details Updated`}
		})
	}else{return Promise.resolve(`Invalid Parameter.`)}

}

// Archive Product [Restriction: Admin]
module.exports.archiveProduct=reqParams=>{
	let prodId = reqParams.productId
	let prodStat = {
		isActive:false,
		modifiedAt:Date.now()
	}
	if(prodId.match(/^[0-9a-fA-F]{24}$/)){
		return Product.findByIdAndUpdate(reqParams.productId,prodStat).then((result,error)=>{
			if(error){
				return `Something went wrong while archiving the product`
			}else {return `Product is archived.`}
		})
	}else{return Promise.resolve(`Invalid Parameter.`)}
}

